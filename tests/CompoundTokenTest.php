<?php declare(strict_types=1);

namespace Judahnator\Lexer\Tests;

use Judahnator\Lexer\Token\CompoundToken;
use Judahnator\Lexer\Token\Token;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Judahnator\Lexer\Token\CompoundToken
 */
final class CompoundTokenTest extends TestCase
{
    public function testCompoundToken(): void
    {
        $token = new CompoundToken(
            new Token('foo', 'FOO'),
            new Token('bar', 'BAR'),
        );

        $this->assertEquals('foobar', $token->getName());
        $this->assertCount(6, $token);
        $this->assertEquals('FOOBAR', $token->getLiteral());
    }
}