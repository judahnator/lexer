<?php declare(strict_types=1);

namespace Judahnator\Lexer\Tests;

use Judahnator\Lexer\Buffer\CharacterBuffer;
use Judahnator\Lexer\Token\CompoundToken;
use Judahnator\Lexer\Token\ConstantTokenIdentifier;
use Judahnator\Lexer\Token\RepeatingTokenIdentifier;
use Judahnator\Lexer\Token\VariableTokenIdentifier;
use Judahnator\Lexer\TokenMismatchException;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Judahnator\Lexer\Token\RepeatingTokenIdentifier
 */
final class RepeatingTokenTest extends TestCase
{
    public function testRepeatingTokens(): void
    {
        $identifier = new RepeatingTokenIdentifier(
            new VariableTokenIdentifier([
                'FOO' => 'foo',
                'BAR' => 'bar',
                'BAZ' => 'baz',
                'BING' => 'bing',
            ]),
            new ConstantTokenIdentifier('COMMA', ',')
        );

        $match = 'foo,bar,baz,bing';
        $token = $identifier->matches(new CharacterBuffer($match));

        $this->assertInstanceOf(CompoundToken::class, $token);
        $this->assertCount(strlen($match), $token);
        $this->assertEquals($match, $token->getLiteral());
    }

    public function testRepeatingTokenException(): void
    {
        $this->expectException(TokenMismatchException::class);

        $identifier = new RepeatingTokenIdentifier(
            new ConstantTokenIdentifier('foo', 'FOO'),
            new ConstantTokenIdentifier('bar', 'BAR'),
            new ConstantTokenIdentifier('baz', 'BAZ'),
        );

        $identifier->matches(new CharacterBuffer('foobar'));
    }
}