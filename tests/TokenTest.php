<?php declare(strict_types=1);

namespace Judahnator\Lexer\Tests;

use Judahnator\Lexer\Token\Token;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Judahnator\Lexer\Token\Token
 */
final class TokenTest extends TestCase
{
    public function testToken(): void
    {
        $token = new Token('name', 'literal');

        $this->assertEquals('name', $token->getName());
        $this->assertCount(7, $token);
        $this->assertEquals('literal', $token->getLiteral());
    }
}