<?php declare(strict_types=1);

namespace Judahnator\Lexer\Tests;

use Judahnator\Lexer\Buffer\Buffer;
use Judahnator\Lexer\Buffer\CharacterBuffer;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Judahnator\Lexer\Buffer\CharacterBuffer
 */
final class CharacterBufferTest extends TestCase
{
    public function testCharacterBuffer(): void
    {
        $characters = new CharacterBuffer('foobar');

        $this->assertEquals(['f', 'o', 'o'], $characters->read(3));
        $this->assertEquals(['b', 'a', 'r'], $characters->read(3, 3));

        $this->assertEquals('foo', $characters->readLiteral(3));
        $this->assertEquals('bar', $characters->readLiteral(3, 3));

        $this->assertEquals('ooba', $characters->slice(1, 4)->readLiteral(4));

        $characters->seek(3);
        $this->assertTrue($characters->valid());
        $this->assertEquals('bar', $characters->readLiteral(3));
        $characters->seek(3);
        $this->assertFalse($characters->valid());
    }

    public function testReadingBeyondCharacterBuffer(): void
    {
        $characters = new CharacterBuffer('foo');
        $beyondLastCharacter = $characters->readOne(3);
        $this->assertEquals(Buffer::EOL, $beyondLastCharacter->getName());
        $this->assertTrue($beyondLastCharacter->isWhitespace());
    }
}