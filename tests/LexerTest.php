<?php declare(strict_types=1);

namespace Judahnator\Lexer\Tests;

use Judahnator\Lexer\Buffer\CharacterBuffer;
use Judahnator\Lexer\Dictionary;
use Judahnator\Lexer\Lexer;
use Judahnator\Lexer\Token\Character;
use Judahnator\Lexer\Token\ConstantTokenIdentifier;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Judahnator\Lexer\Lexer
 */
final class LexerTest extends TestCase
{
    public function testTokenize(): void
    {
        $dictionary = new Dictionary(
            new ConstantTokenIdentifier('FOO_TOKEN', 'foo'),
            new ConstantTokenIdentifier('BAR_TOKEN', 'bar'),
        );
        $lexer = new Lexer($dictionary);
        $tokens = $lexer->tokenize(new CharacterBuffer('foo bar'));
        $this->assertEquals('foo', $tokens->readOne(0)->getLiteral());
        $this->assertEquals('bar', $tokens->readOne(1)->getLiteral());
    }
}
