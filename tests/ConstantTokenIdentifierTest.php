<?php declare(strict_types=1);

namespace Judahnator\Lexer\Tests;

use Judahnator\Lexer\Buffer\CharacterBuffer;
use Judahnator\Lexer\Token\ConstantTokenIdentifier;
use Judahnator\Lexer\Token\Token;
use Judahnator\Lexer\TokenMismatchException;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Judahnator\Lexer\Token\ConstantTokenIdentifier
 */
final class ConstantTokenIdentifierTest extends TestCase
{
    public function testConstantTokenIdentifier(): void
    {
        $identifier = new ConstantTokenIdentifier('name', 'literal');

        $match = $identifier->matches(new CharacterBuffer('literal'));
        $this->assertInstanceOf(Token::class, $match);
        $this->assertEquals('name', $match->getName());
        $this->assertCount(7, $match);
        $this->assertEquals('literal', $match->getLiteral());
    }

    public function testConstantTokenIdentifierException(): void
    {
        $this->expectException(TokenMismatchException::class);
        (new ConstantTokenIdentifier('name', 'literal'))->matches(new CharacterBuffer('asdf'));
    }
}