<?php declare(strict_types=1);

namespace Judahnator\Lexer\Tests;

use Judahnator\Lexer\Buffer\CharacterBuffer;
use Judahnator\Lexer\Contract\DictionaryInterface;
use Judahnator\Lexer\Contract\LexerInterface;
use Judahnator\Lexer\Dictionary;
use Judahnator\Lexer\Lexer;
use Judahnator\Lexer\Token\ConstantTokenIdentifier;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Judahnator\Lexer\Buffer\TokenBuffer
 */
final class TokenBufferTest extends TestCase
{
    private static function lexer(): LexerInterface
    {
        return new Lexer(
            new Dictionary(
                new ConstantTokenIdentifier('FOO_TOKEN', 'foo'),
                new ConstantTokenIdentifier('BAR_TOKEN', 'bar'),
            )
        );
    }

    public function testTokenBuffer(): void
    {
        $tokens = self::lexer()->tokenize(new CharacterBuffer('foo bar'));

        $this->assertEquals('FOO_TOKEN', $tokens->readOne()->getName());
        $this->assertEquals('BAR_TOKEN', $tokens->slice(1)->readOne()->getName());
        $tokens->seek(1);
        $this->assertEquals('BAR_TOKEN', $tokens->readOne()->getName());
        $this->assertTrue($tokens->valid());
        $tokens->seek(1);
        $this->assertFalse($tokens->valid());
    }

    public function testIteration(): void
    {
        $tokens = iterator_to_array(self::lexer()->tokenize(new CharacterBuffer('foo bar')));

        $this->assertCount(2, $tokens);
        $this->assertEquals('FOO_TOKEN', $tokens[0]->getName());
        $this->assertEquals('BAR_TOKEN', $tokens[1]->getName());
    }
}