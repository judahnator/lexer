<?php declare(strict_types=1);

namespace Judahnator\Lexer\Tests;

use JetBrains\PhpStorm\Pure;
use Judahnator\Lexer\Buffer\CharacterBuffer;
use Judahnator\Lexer\Dictionary;
use Judahnator\Lexer\Token\ConstantTokenIdentifier;
use Judahnator\Lexer\Token\RepeatingTokenIdentifier;
use Judahnator\Lexer\Token\VariableTokenIdentifier;
use Judahnator\Lexer\TokenMismatchException;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Judahnator\Lexer\Dictionary
 */
final class DictionaryTest extends TestCase
{
    #[Pure] public function testDictionary(): void
    {
        $dictionary = new Dictionary(
            new ConstantTokenIdentifier('FOO_TOKEN', 'foo'),
            new ConstantTokenIdentifier('BAR_TOKEN', 'bar'),
        );

        $this->assertEquals('FOO_TOKEN', $dictionary->match(new CharacterBuffer('foo'))->getName());
        $this->assertEquals('BAR_TOKEN', $dictionary->match(new CharacterBuffer('bar'))->getName());
    }

    #[Pure] public function testDictionaryException(): void
    {
        $this->expectExceptionObject(new TokenMismatchException('Unknown value encountered'));
        $dictionary = new Dictionary(
            new ConstantTokenIdentifier('FOO_TOKEN', 'foo'),
            new ConstantTokenIdentifier('BAR_TOKEN', 'bar'),
        );
        $dictionary->match(new CharacterBuffer('baz'));
    }

    public function testDictionaryStringable(): void
    {
        $dictionary = new Dictionary(
            new RepeatingTokenIdentifier(
                new ConstantTokenIdentifier('FOO_TOKEN', 'foo'),
                new ConstantTokenIdentifier('BAR_TOKEN', 'bar'),
            ),
            new VariableTokenIdentifier([
                'BAZ_TOKEN' => 'baz',
                'BING_TOKEN' => 'bing',
            ]),
        );
        $this->assertEquals(
            '(...foo,bar)[baz|bing]',
            (string)$dictionary
        );
    }
}
