<?php declare(strict_types=1);

namespace Judahnator\Lexer\Contract;

use Judahnator\Lexer\TokenMismatchException;
use Stringable;

interface TokenIdentifierInterface extends Stringable
{
    /**
     * Determines if this token is represented by the given buffer.
     * If so a new instance is returned, otherwise throw a TokenMismatchException.
     * @param BufferInterface $characters
     * @return TokenInterface
     * @throws TokenMismatchException
     */
    public function matches(BufferInterface $characters): TokenInterface;
}