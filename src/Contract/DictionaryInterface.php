<?php

namespace Judahnator\Lexer\Contract;

use Judahnator\Lexer\TokenMismatchException;
use Stringable;

interface DictionaryInterface extends Stringable
{
    /**
     * @param TokenIdentifierInterface ...$identifiers
     */
    public function __construct(TokenIdentifierInterface ...$identifiers);

    /**
     * @param BufferInterface $buffer
     * @return TokenInterface
     * @throws TokenMismatchException
     */
    public function match(BufferInterface $buffer): TokenInterface;
}