<?php declare(strict_types=1);

namespace Judahnator\Lexer\Contract;

interface ValueTokenIdentifierInterface extends TokenIdentifierInterface
{
    public function matches(BufferInterface $characters): ValueTokenInterface;
}
