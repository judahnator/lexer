<?php declare(strict_types=1);

namespace Judahnator\Lexer\Contract;

use JetBrains\PhpStorm\Pure;

interface ValueTokenInterface extends TokenInterface
{
    /**
     * Returns the value represented by this token.
     */
    #[Pure] public function getValue();
}
