<?php declare(strict_types=1);

namespace Judahnator\Lexer\Contract;

use Countable;
use JetBrains\PhpStorm\Pure;

interface TokenInterface extends Countable
{
    /**
     * Provides the token length.
     *
     * @return int
     */
    #[Pure] public function count(): int;

    /**
     * Returns the literal value of the token.
     *
     * @return string
     */
    public function getLiteral(): string;

    /**
     * Returns the name of this token.
     *
     * @return string
     */
    public function getName(): string;

    /**
     * Determines if this token represents whitespace, and should be skipped by the Lexer.
     *
     * @return bool
     */
    public function isWhitespace(): bool;
}