<?php declare(strict_types=1);

namespace Judahnator\Lexer\Contract;

use Judahnator\Lexer\Buffer\TokenBuffer;
use Judahnator\Lexer\TokenMismatchException;

/**
 * @template T
 */
interface LexerInterface
{
    /**
     * The Lexer tokenization process.
     *
     * @param BufferInterface<T> $tokens
     * @return TokenBuffer
     * @throws TokenMismatchException
     */
    public function tokenize(BufferInterface $tokens): TokenBuffer;
}