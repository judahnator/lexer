<?php declare(strict_types=1);

namespace Judahnator\Lexer\Contract;

/**
 * Interface Buffer
 * @package judahnator\Lua\Buffers
 */
interface BufferInterface
{
    /**
     * @param int $length
     * @param int $offset
     * @return array<TokenInterface>
     */
    public function read(int $length = 1, int $offset = 0): array;

    /**
     * @param int $length
     * @param int $offset
     * @return string
     */
    public function readLiteral(int $length = 1, int $offset = 0): string;

    /**
     * @param int $offset
     * @return ?TokenInterface
     */
    public function readOne(int $offset = 0): ?TokenInterface;

    public function seek(int $length): void;

    public function slice(int $offset, int $length = null): self;

    public function valid(): bool;
}