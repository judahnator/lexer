<?php declare(strict_types=1);

namespace Judahnator\Lexer\Token;

use JetBrains\PhpStorm\Pure;
use Judahnator\Lexer\Contract\TokenInterface;

/**
 * Represents a list of tokens.
 * @see RepeatingTokenIdentifier
 */
final readonly class CompoundToken implements TokenInterface
{
    /** @var TokenInterface[] $tokens */
    public array $tokens;

    public function __construct(TokenInterface ...$tokens)
    {
        $this->tokens = $tokens;
    }

    /**
     * @inheritDoc
     */
    #[Pure] public function count(): int
    {
        return array_reduce(
            $this->tokens,
            static fn (int $carry, TokenInterface $token): int => $carry + $token->count(),
            0,
        );
    }

    /**
     * @inheritDoc
     */
    public function getLiteral(): string
    {
        return array_reduce(
            $this->tokens,
            static fn (string $carry, TokenInterface $token): string => $carry . $token->getLiteral(),
            '',
        );
    }

    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return array_reduce(
            $this->tokens,
            static fn (string $carry, TokenInterface $token): string => $carry . $token->getName(),
            '',
        );
    }

    public function isWhitespace(): bool
    {
        return false;
    }
}