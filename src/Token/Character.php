<?php declare(strict_types=1);

namespace Judahnator\Lexer\Token;

use JetBrains\PhpStorm\Pure;
use Judahnator\Lexer\Contract\TokenInterface;
use Stringable;

final readonly class Character implements TokenInterface, Stringable
{
    public function __construct(public string $char)
    {
    }

    public function __toString(): string
    {
        return $this->char;
    }

    #[Pure] public function count(): int
    {
        return strlen($this->char);
    }

    public function getLiteral(): string
    {
        return $this->char;
    }

    public function getName(): string
    {
        return 'Character';
    }

    public function isWhitespace(): bool
    {
        return ctype_space($this->char) || $this->char === '' || $this->char === ';';
    }
}