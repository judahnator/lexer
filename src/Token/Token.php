<?php declare(strict_types=1);

namespace Judahnator\Lexer\Token;

use JetBrains\PhpStorm\Pure;
use Judahnator\Lexer\Contract\TokenInterface;

final readonly class Token implements TokenInterface
{
    public function __construct(
        private string $tokenName,
        private string $literal,
        private bool   $isWhitespace = false,
    ) {}

    /**
     * @inheritDoc
     */
    #[Pure] public function count(): int
    {
        return strlen($this->literal);
    }

    /**
     * @inheritDoc
     */
    public function getLiteral(): string
    {
        return $this->literal;
    }

    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return $this->tokenName;
    }

    public function isWhitespace(): bool
    {
        return $this->isWhitespace;
    }
}