<?php declare(strict_types=1);

namespace Judahnator\Lexer\Token;

use Judahnator\Lexer\Contract\BufferInterface;
use Judahnator\Lexer\Contract\TokenIdentifierInterface;
use Judahnator\Lexer\Contract\TokenInterface;
use Judahnator\Lexer\TokenMismatchException;

final readonly class VariableTokenIdentifier implements TokenIdentifierInterface
{
    /** @var array<string, TokenInterface> */
    private array $tokens;

    /**
     * @param array $tokens
     */
    public function __construct(array $tokens)
    {
        $list = [];
        foreach ($tokens as $identifier => $literal) {
            $list[$literal] = new Token($identifier, $literal);
        }
        $this->tokens = $list;
    }

    public function __toString(): string
    {
        $tokens = implode('|', array_map(
            static fn (TokenInterface $token): string => $token->getLiteral(),
            $this->tokens
        ));
        return "[$tokens]";
    }

    public function matches(BufferInterface $characters): TokenInterface
    {
        foreach ($this->tokens as $literal => $token) {
            if ($characters->readLiteral(strlen($literal)) === $literal) {
                return $token;
            }
        }
        throw new TokenMismatchException();
    }
}
