<?php declare(strict_types=1);

namespace Judahnator\Lexer\Token;

use Judahnator\Lexer\Contract\BufferInterface;
use Judahnator\Lexer\Contract\TokenIdentifierInterface;
use Judahnator\Lexer\TokenMismatchException;

final readonly class RepeatingTokenIdentifier implements TokenIdentifierInterface
{
    private array $tokens;

    public function __construct(
        TokenIdentifierInterface ...$tokens
    )
    {
        $this->tokens = $tokens;
    }

    public function __toString(): string
    {
        return '(...' . implode(',', $this->tokens) . ')';
    }

    /**
     * @inheritDoc
     */
    public function matches(BufferInterface $characters): CompoundToken
    {
        $matched = [];
        $offset = 0;
        while (true) {
            try {
                foreach ($this->tokens as $identifier) {
                    $matched[] = $match = $identifier->matches($characters->slice($offset));
                    $offset += $match->count();
                }
            } catch (TokenMismatchException) {
                break;
            }
        }
        if (count($matched) < count($this->tokens)) {
            throw new TokenMismatchException();
        }
        return new CompoundToken(...$matched);
    }
}