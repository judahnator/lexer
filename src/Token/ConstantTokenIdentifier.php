<?php declare(strict_types=1);

namespace Judahnator\Lexer\Token;

use JetBrains\PhpStorm\Pure;
use Judahnator\Lexer\Contract\BufferInterface;
use Judahnator\Lexer\Contract\TokenIdentifierInterface;
use Judahnator\Lexer\Contract\TokenInterface;
use Judahnator\Lexer\TokenMismatchException;

final readonly class ConstantTokenIdentifier implements TokenIdentifierInterface
{
    private TokenInterface $token;

    #[Pure] public function __construct(string $tokenName, string $literal)
    {
        $this->token = new Token($tokenName, $literal);
    }

    public function __toString(): string
    {
        return $this->token->getLiteral();
    }

    /**
     * @inheritDoc
     */
    public function matches(BufferInterface $characters): TokenInterface
    {
        if ($characters->readLiteral($this->token->count()) === $this->token->getLiteral()) {
            return $this->token;
        }
        throw new TokenMismatchException();
    }
}
