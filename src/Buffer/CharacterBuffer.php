<?php declare(strict_types=1);

namespace Judahnator\Lexer\Buffer;

use JetBrains\PhpStorm\Pure;
use Judahnator\Lexer\Token\Character;

final class CharacterBuffer extends Buffer
{
    public function __construct(private string $characters)
    {
    }

    #[Pure] public function read(int $length = 1, int $offset = 0): array
    {
        return array_map(
            static fn (string $char): Character => new Character($char),
            str_split($this->readLiteral($length, $offset))
        );
    }

    #[Pure] public function readLiteral(int $length = 1, int $offset = 0): string
    {
        return substr($this->characters, $offset, $length);
    }

    public function seek(int $length): void
    {
        $this->characters = substr($this->characters, $length);
    }

    #[Pure] public function slice(int $offset, int $length = null): self
    {
        return new self(substr($this->characters, $offset, $length));
    }

    #[Pure] public function valid(): bool
    {
        return $this->characters !== '';
    }
}