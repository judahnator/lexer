<?php declare(strict_types=1);

namespace Judahnator\Lexer\Buffer;

use Generator;
use IteratorAggregate;
use Judahnator\Lexer\Contract\BufferInterface;
use Judahnator\Lexer\Contract\TokenInterface;
use Judahnator\Lexer\Token\Token;

/**
 * Interface Buffer
 * @package judahnator\Lua\Buffers
 */
abstract class Buffer implements BufferInterface, IteratorAggregate
{
    public const EOL = 'EOL';

    /**
     * @return Generator<int, TokenInterface>
     */
    public function getIterator(): Generator
    {
        $offset = 0;
        while (($slice = $this->slice($offset))->valid()) {
            yield $offset++ => $slice->readOne();
        }
    }

    /**
     * @param int $offset
     * @return TokenInterface
     */
    final public function readOne(int $offset = 0): TokenInterface
    {
        return $this->read(offset: $offset)[0] ?? new Token(self::EOL, '', true);
    }
}