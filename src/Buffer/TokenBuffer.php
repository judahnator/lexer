<?php declare(strict_types=1);

namespace Judahnator\Lexer\Buffer;

use Generator;
use Iterator;
use Judahnator\Lexer\Contract\TokenInterface;

final class TokenBuffer extends Buffer
{
    private array $buffer = [];

    /**
     * TokenBuffer constructor.
     * @param Iterator<TokenInterface> $tokens
     */
    public function __construct(private readonly Iterator $tokens)
    {
    }

    public function read(int $length = 1, int $offset = 0): array
    {
        $lookahead = $length + $offset;
        while (count($this->buffer) < $lookahead && $this->tokens->valid()) {
            $this->buffer[] = $this->tokens->current();
            $this->tokens->next();
        }
        return array_pad(array_slice($this->buffer, $offset, $length), $length, null);
    }

    public function readLiteral(int $length = 1, int $offset = 0): string
    {
        return implode('', $this->read($offset, $length));
    }

    public function seek(int $length): void
    {
        $this->buffer = array_slice($this->buffer, $length, count($this->buffer) - $length);
    }

    public function slice(int $offset, int $length = null): self
    {
        return new self((function () use ($offset, $length): Generator {
            while (
                $this->valid() &&
                ($token = $this->readOne($offset++))->getName() !== Buffer::EOL
            ) {
                yield $token;
                if (!is_null($length) && --$length === 0) {
                    break;
                }
            }
        })());
    }

    public function valid(): bool
    {
        return !empty($this->buffer) || $this->tokens->valid();
    }

}