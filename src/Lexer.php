<?php declare(strict_types=1);

namespace Judahnator\Lexer;

use Generator;
use Judahnator\Lexer\Buffer\TokenBuffer;
use Judahnator\Lexer\Contract\BufferInterface;
use Judahnator\Lexer\Contract\DictionaryInterface;
use Judahnator\Lexer\Contract\LexerInterface;

readonly class Lexer implements LexerInterface
{
    public function __construct(
        public DictionaryInterface $dictionary = new Dictionary()
    ) {}

    public function tokenize(BufferInterface $tokens): TokenBuffer
    {
        return new TokenBuffer((function () use ($tokens): Generator {
            while ($tokens->valid()) {
                if ($tokens->readOne()->isWhitespace()) {
                    $tokens->seek($tokens->readOne()->count());
                    continue;
                }
                $next = $this->dictionary->match($tokens);
                yield $next;
                $tokens->seek($next->count());
            }
        })());
    }
}