<?php declare(strict_types=1);

namespace Judahnator\Lexer;

use Judahnator\Lexer\Contract\BufferInterface;
use Judahnator\Lexer\Contract\DictionaryInterface;
use Judahnator\Lexer\Contract\TokenIdentifierInterface;
use Judahnator\Lexer\Contract\TokenInterface;

final readonly class Dictionary implements DictionaryInterface
{
    /** @var TokenIdentifierInterface[] */
    private array $identifiers;

    /**
     * @param TokenIdentifierInterface ...$identifiers
     */
    public function __construct(TokenIdentifierInterface ...$identifiers)
    {
        $this->identifiers = $identifiers;
    }

    public function __toString(): string
    {
        return implode('', $this->identifiers);
    }

    public function match(BufferInterface $buffer): TokenInterface
    {
        foreach ($this->identifiers as $token) {
            try {
                return $token->matches($buffer);
            } catch (TokenMismatchException) {
                continue;
            }
        }
        throw new TokenMismatchException('Unknown value encountered');
    }
}