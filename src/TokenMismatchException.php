<?php declare(strict_types=1);

namespace Judahnator\Lexer;

use RuntimeException;

final class TokenMismatchException extends RuntimeException
{
}